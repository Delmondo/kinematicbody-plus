extends KinematicBody
class_name KinematicBodyPlus

# collision variables
var colliders: Array = []

# floor variables
var on_floor: bool = false
var on_floor_body: int = -1
var floor_velocity: Vector3 = Vector3()
var floor_normal: Vector3 = Vector3()

# misc collision variables
var on_ceiling: bool = false
var on_wall: bool = false


func is_on_floor() -> bool:
	return on_floor


func get_floor_normal() -> Vector3:
	return floor_normal


func is_on_ceiling() -> bool:
	return on_ceiling


func is_on_wall() -> bool:
	return on_wall


const FLOOR_ANGLE_THRESHOLD: float = 0.01


func move_and_slide_plus(	p_linear_velocity: Vector3,
							p_up_direction: Vector3 = Vector3(),
							p_stop_on_slope: bool = false,
							p_max_slides: int = 4,
							p_floor_max_angle: float = deg2rad(46.0),
							p_infinite_inertia: bool = false) -> Dictionary:

	var body_velocity: Vector3 = p_linear_velocity
	var body_velocity_normal: Vector3 = body_velocity.normalized()

	var move_lock: Vector3 = Vector3(float(!move_lock_x), float(!move_lock_y), float(!move_lock_z))

	body_velocity *= move_lock

	# author: Hack in order to work with calling from _process as well as from _physics_process; calling from thread is risky
	var motion: Vector3 = (floor_velocity + body_velocity) * (get_physics_process_delta_time() if Engine.is_in_physics_frame() else get_process_delta_time())

	on_floor = false
	on_floor_body = -1
	on_ceiling = false
	on_wall = false
	colliders.clear()
	floor_normal = Vector3()
	floor_velocity = Vector3()

	var collision: KinematicCollision = null
	while p_max_slides:
		var found_collision: bool = false

		for i in range(1):
			var collided: bool = false
			if i == 0:
				collision = move_and_collide(motion, p_infinite_inertia)
				collided = !!collision
				if !collided:
					motion = Vector3() # author: clear because no collision happened and motion completed
			# this is where the else block with the raycast separation goes

			if collided:
				found_collision = true

				colliders.push_back(collision)
				motion = collision.remainder

				if p_up_direction == Vector3():
					# author: all is a wall
					on_wall = true
				else:
					if acos(collision.normal.dot(p_up_direction)) <= p_floor_max_angle + FLOOR_ANGLE_THRESHOLD: # author: floor
						on_floor = true
						floor_normal = collision.normal
						on_floor_body = collision.collider_id
						floor_velocity = collision.collider_velocity

						if p_stop_on_slope:
							var gt: Transform = global_transform
							gt.origin -= collision.travel.slide(p_up_direction)
							global_transform = gt
							return { "body_velocity": Vector3(), "collision": collision }
					elif acos(collision.normal.dot(-p_up_direction)) <= p_floor_max_angle + FLOOR_ANGLE_THRESHOLD: # author: ceiling
						on_ceiling = true
					else:
						on_wall = true

					motion = motion.slide(collision.normal)
					body_velocity = body_velocity.slide(collision.normal)

					body_velocity *= move_lock

		if !found_collision || motion == Vector3():
			break

		--p_max_slides

	return { "body_velocity": body_velocity, "collision": collision }


func move_and_slide(	p_linear_velocity: Vector3,
						p_up_direction: Vector3 = Vector3(),
						p_stop_on_slope: bool = false,
						p_max_slides: int = 4,
						p_floor_max_angle: float = deg2rad(46.0),
						p_infinite_inertia: bool = false) -> Vector3:

	return move_and_slide_plus(
		p_linear_velocity, p_up_direction, p_stop_on_slope, p_max_slides,
		p_floor_max_angle, p_infinite_inertia
	).body_velocity


func move_and_slide_with_snap_plus(	p_linear_velocity: Vector3,
									p_snap: Vector3,
									p_up_direction: Vector3 = Vector3(),
									p_stop_on_slope: bool = false,
									p_max_slides: int = 4,
									p_floor_max_angle: float = deg2rad(45.0),
									p_infinite_inertia: bool = true) -> Dictionary:

	var was_on_floor: bool = on_floor

	var ret: Vector3 = move_and_slide(
		p_linear_velocity, p_up_direction, p_stop_on_slope, p_max_slides,
		p_floor_max_angle, p_infinite_inertia
	)

	if !was_on_floor || p_snap == Vector3():
		return { "body_velocity": ret, "snapped": false }

	var col: KinematicCollision = null
	var col_travel: Vector3 = Vector3()
	var gt: Transform = global_transform
	var apply: bool = true

	col = move_and_collide(p_snap, p_infinite_inertia, false, true)

	if col:
		col_travel = col.travel

		if p_up_direction != Vector3():
			if acos(p_up_direction.normalized().dot(col.normal)) < p_floor_max_angle:
				on_floor = true
				floor_normal = col.normal
				on_floor_body = col.collider_id
				floor_velocity = col.collider_velocity
				if p_stop_on_slope:
					# author: move and collide may stray the object a bit because of pre-unstucking,
					# so only ensure that the motion happens on floor direction in this case.
					col_travel = col.travel.project(p_up_direction)
			else:
				apply = false # author: snapped with floor direction, but did not snap to a floor, do not snap.

		if apply:
			gt.origin += col_travel
			global_transform = gt

	return { "body_velocity": ret, "snapped": apply }


func move_and_slide_with_snap(	p_linear_velocity: Vector3,
									p_snap: Vector3,
									p_up_direction: Vector3 = Vector3(),
									p_stop_on_slope: bool = false,
									p_max_slides: int = 4,
									p_floor_max_angle: float = deg2rad(45.0),
									p_infinite_inertia: bool = true) -> Vector3:

	return move_and_slide_with_snap_plus(
		p_linear_velocity, p_snap, p_up_direction, p_stop_on_slope,
		p_max_slides, p_floor_max_angle, p_infinite_inertia
	).body_velocity